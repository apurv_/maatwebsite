<?php

namespace App\Exports;
use DB;
use Excel;
use App\Models\CustomerMaster;
use Maatwebsite\Excel\Concerns\FromCollection;


class ExportUser implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
//        return CustomerMaster::all();
        $cust = DB::select("
        SELECT RIGHT(customer_mobile,9) AS cm FROM customer_master GROUP BY cm HAVING COUNT(cm) > 1
        ");

        $dup = CustomerMaster::select([
            'id',
            'customer_fname',
            'customer_lname',
            'customer_mobile',
            'created_at',
            'updated_at',
        ])
            ->where(function ($q) use ($cust) {
                foreach ($cust as $data) {
                    $q->orWhere('customer_mobile', 'like', '%' . $data->cm . '%');
                }
            })
            ->get();

        return $dup;
    }
}
