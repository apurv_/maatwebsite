<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use App\Models\CustomerMaster;
use App\Exports\ExportUser;
class ExlExport extends Controller
{
    function index(){

        $cust = DB::select("
        SELECT RIGHT(customer_mobile,9) AS cm FROM customer_master GROUP BY cm HAVING COUNT(cm) > 1
        ");

        $dup = CustomerMaster::select([
            'id',
            'customer_fname',
            'customer_lname',
            'customer_mobile',
            'created_at',
            'updated_at',
        ])
            ->where(function ($q) use ($cust) {
                foreach ($cust as $data) {
                    $q->orWhere('customer_mobile', 'like', '%' . $data->cm . '%');
                }
            })
            ->get();
//            ->get()->toArray();


        return view('export_excel')->with('dup', $dup);
    }

    function excel(){


//        $cust_array[] = array('Customer id', 'customer_fname', 'customer_lname', 'customer_mobile', 'created_at', 'updated_at');
//        foreach ($dup as $d){
//            $cust_array[] = array(
//                'Customer id' => $d->id,
//                'First name' => $d->customer_fname,
//                'Last name' => $d->customer_lname,
//                'Mobile' => $d->customer_mobile,
//                'Created at' => $d->created_at,
//                'Updated at' => $d->updated_at
//
//            );
//        }
//        Excel::store('Customer data', function($excel) use($cust_array){
//            $excel -> setTitle('Customer Data');
//            $excel -> sheet('Customer Data', function($sheet) use ($cust_array){
//                $sheet->fromArray($cust_array, null, 'A1', false, false);
//            });
//        })->download('xlsx');

        return Excel::download(new ExportUser, 'export.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }
}
